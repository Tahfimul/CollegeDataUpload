package com.example.successway.collegedataupload;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView numCurrent;
    TextView numLeft;
    Button refresh;
    FirebaseDatabase mRef;
    static String current, Left;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numCurrent = (TextView) findViewById(R.id.numCurrent);
        numLeft = (TextView) findViewById(R.id.numLeft);
        refresh = (Button) findViewById(R.id.refresh);
        FirebaseApp.initializeApp(MainActivity.this);
        mRef = FirebaseDatabase.getInstance("https://successway18.firebaseio.com/");
        new CollegeDataUpload().execute();
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });

    }

    /**
     * Created by Home-PC2 on 1/14/2018.
     */

    private class CollegeDataUpload extends AsyncTask<Void, Void, Void> {
        private Elements collegeNames, collegeLocations, collegeTuition, collegeAcceptance, collegeCode;
        ArrayList<String> colleges = new ArrayList<>();
        ArrayList<String> collegesLinks = new ArrayList<>();
        ArrayList<String> locations = new ArrayList<>();
        ArrayList<String> tuition = new ArrayList<>();
        ArrayList<String> admissions = new ArrayList<>();
        ArrayList<String> acceptedSorted = new ArrayList<>();
        String CollegeCode;
        ArrayList<String> collegecode = new ArrayList<>();
        org.jsoup.nodes.Document HTML;
        boolean exists;
        int o;
        String getCollegeCode;
        @Override
        protected Void doInBackground(Void... params) {
            try {
                HTML = Jsoup.connect("https://www.internationalstudent.com/school-search/school/search/?School%5BsearchProgram%5D=").get();
                collegeCode = HTML.select("option");
                for (org.jsoup.nodes.Element option : collegeCode) {
                    collegecode.add(option.attr("value"));
                }
                for (int d = collegecode.size() - 1; d > 67; d--) {
                    collegecode.remove(d);
                }

                for (o = 0; o < collegecode.size(); o++) {
                    current = collegecode.get(o);
                    Left = String.valueOf(collegecode.size() - o);
                    getCollegeCode = collegecode.get(o);
                    check();
                    if (exists)
                    {
                        System.out.println("Exists yes yes");
                        exists = false;
                        if (o < 67)
                            o++;
                        getCollegeCode = collegecode.get(o);
                        check();
                    }
                    else {
                        // Connect to the web site with list of colleges
                        HTML = Jsoup.connect("https://www.internationalstudent.com/school-search/school/search/?School%5BsearchProgram%5D=" + collegecode.get(o)).get();
                        //Find a tags with specific classes which contain the name of schools
                        collegeNames = HTML.select("a.font-bitter.text-left.text-danger.mb-2.mb-lg-0[href]");
                        for (org.jsoup.nodes.Element a : collegeNames) {
                            //Add college names to colleges ArrayList
                            colleges.add(a.text());
                            //Add links to every college page to collegeLinks ArrayList
                            collegesLinks.add("https://www.internationalstudent.com" + a.attr("href"));
                        }
                        //Find small tags with specific classes which contain the location of schools
                        collegeLocations = HTML.select("small.text-dark.text-right");
                        for (org.jsoup.nodes.Element small : collegeLocations) {
                            //Add college locations to locations ArrayList
                            locations.add(small.text());
                            System.out.println(locations);
                        }

                        for (int i = 0; i < colleges.size(); i++) {
                            // Connect to the web site with specifics of colleges
                            HTML = Jsoup.connect(collegesLinks.get(i)).get();
                            //Find p tags with specific classes which contain the college tuition amounts
                            collegeTuition = HTML.select("p.total-cost.f-18");
                            for (org.jsoup.nodes.Element p : collegeTuition) {
                                //Add tuition amounts to tuition ArrayList
                                tuition.add(p.text());
                            }
                            switch (collegeTuition.size()) {
                                case 0:
                                    tuition.add("N/A");
                                    break;
                                case 1:
                                    break;
                                default:
                                    for (int j = 0; j < collegeTuition.size(); j++) {
                                        tuition.remove(tuition.size() - 1);
                                    }
                                    tuition.add("N/A");
                                    break;
                            }
                            collegeAcceptance = HTML.select("p.f-18");
                            for (Element p : collegeAcceptance) {
                                //Add all values of admissions (Total Students, Total Faculty, Tuition, Total Applicants,
                                //Admitted and Enrolled) to acceptance ArrayList
                                admissions.add(p.text());

                            }

                            switch (collegeAcceptance.size()) {
                                case 0:
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    break;
                                case 1:
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    break;
                                case 2:
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    break;
                                case 3:
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    break;
                                case 4:
                                    admissions.add("");
                                    admissions.add("");
                                    break;
                                case 5:
                                    admissions.add("");
                                    break;
                                case 6:
                                    break;
                                default:
                                    for (int j = 0; j < collegeAcceptance.size(); j++) {
                                        admissions.remove(admissions.size() - 1);
                                    }
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    admissions.add("");
                                    break;
                            }
                            System.out.println("Card num" + i + 1);
                            //Sort from admissions ArrayList and add acceptance rate (Admitted) to acceptance ArrayList
                            acceptedSorted.add(admissions.get((4) + (6 * i)));


                        }

                        for (int i = 0; i < colleges.size(); i++) {
                            if (acceptedSorted.get(i).equals(""))
                                acceptedSorted.add(i, "N/A");
                            mRef.getReference().child("CollegeCodes").child(collegecode.get(o)).child(colleges.get(i)).child("Location").setValue(locations.get(i));
                            mRef.getReference().child("CollegeCodes").child(collegecode.get(o)).child(colleges.get(i)).child("Link").setValue(collegesLinks.get(i));
                            mRef.getReference().child("CollegeCodes").child(collegecode.get(o)).child(colleges.get(i)).child("Tuition").setValue(tuition.get(i));
                            mRef.getReference().child("CollegeCodes").child(collegecode.get(o)).child(colleges.get(i)).child("Accepted").setValue(acceptedSorted.get(i));

                        }
                    }
                }
            }
            //If something goes wrong
            catch (IOException e) {


            }

            return null;
        }

        //If parse is successful
        @Override
        protected void onPostExecute(Void result) {

            System.out.println(colleges.size());
        }


        void check()
        {
            mRef.getReference().child("CollegeCodes").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(getCollegeCode))
                        exists = true;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    void update()
    {
        numCurrent.setText(current);
        numLeft.setText(Left);
    }
}
